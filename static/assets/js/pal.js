(function ($) {
	var $lg = $('#main.pal');
	$lg.lightGallery({
		selector: '.thumb',
		mode: 'lg-fade',
		speed: 300,
		preload: 3,
		showThumbByDefault: false,
		thumbnail: false,
		hideBarsDelay: 1000,
		controls: true,
		hideControlOnEnd: false,
		loop: true,
		autoplay: false,
		pause: 3000,
		progressBar: false,
		escKey: true,
		keyPress: true,
		mousewheel: true,
		closable: false,
		counter: true,
		enableSwipe: true,
		enableDrag: false,
		videoAutoplay: false,
		download: false,
		fullScreen: true,
		googlePlus: false,
		pinterest: false,
	});

	var lg_data = $lg.data('lightGallery');

	$lg.on('onBeforeSlide.lg', function (event, prevIndex, index) {
		$('.lg-backdrop')
			.css('backdrop-filter', 'blur(10px)')
			.css('background-color', 'rgba(4, 4, 4, 0.9');
	});

	$lg.on('onAfterOpen.lg', function () {
		$lg.data('lightGallery').modules.fullscreen.requestFullscreen();
	});

})(jQuery);
