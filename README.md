# PAL

**P**hoto **AL**bum (PAL) is a gallery theme for Hugo.

It is a theme for hosting [the personal photos website](http://www.didenko.com) for family and friends. As such it has a minimal needed set of features. No backward compatibility with old browsers is considered.

The theme uses  Hugo's _lists_ feature to generate the nested galleries and the [lightGallery](https://github.com/sachinchoolur/lightGallery/) library for photo slideshows.

## Configuration

Here is the list of configuration options used by the theme in the **params** section, in addition to the Hugo's configuration:

* **author**: used to set the _author_ html meta
* **keywords**: used to set the _keywords_ html meta
* **title**: a title of the information (aka About) pop-up
* **about**: an array of strings to go into the information (aka About) pop-up
* **social**: an array of iconified references to be included at the bottom of the information (aka About) pop-up. Each reference object has the fields:
    * **icon**: FontAwesome styles, e.g. "`fas fa-rss`"
    * **label**: alt-label for the HTML link
    * **url**: a destination URL of the HTML link

## Galleries and Albums

The theme deals with lists of albums, aka galleries, and albums themselves, which are really just Hugo-compliant pages of type `pal` with thumbnails. Normally in my application such pages are sequences of photo and notes shortcodes.

### Galleries

The theme is tested with two-level galleries. The first level is a collection of album `.md` files and directories with `_index.md` files in them for sub-galleries. Sub-galleries have to have a `_index.md` file, and are expected to have multiple album `.md` files in them.

In either case, use the following front matter entries for albums so that the albums show correctly in galleries:

* **type**: required to be set to `pal` to be processed by the theme.
* **thumb**: required website path to the album's thumbnail in the gallery. It will be center-cut to fill a square.
* **date**: optional date in "YYYY-MM-DD" format. Normally albums in a gallery are sorted with newest on the top according to Hugo rules. However, in sub-galleries one it is possible to reverse the order (see *timeflow*).
* **title**: a string which will be overlayed on the album's thumbnail in a gallery, and be the album's header line in the album's view.
* **timeflow**: if specified as `forward`, sorts the sub-gallery's albums as oldest on the top.

### Album body

Albums are generally "normal" Hugo pages, with the special processing for image slideshows and notes. The provided styling attempts to:

1. Separate the page into sections.

    Use the `<hr>` element, aka `-----` in Markdown between your intended sections.

1. Scale and wrap notes and photo thumbnails within sections to fit a browser window.

1. Launch a _lightGallery_ slideshow, when clicked on a thumbnail - it opens in a full-screen, no auto-play mode by default.

1. Display navigation to the website homepage ("home" icon in the top left of the page)

1. Display navigation to a sub-gallery ("arrow-up" icon near the "home" icon, if applicable)

The maximum size of thumbnails is 400 pixels and it smoothly scales down to 100 pixels or 8em, whichever is larger.

The thumbnail shortcode is self-explanatory:

    {{< photo path="path/to/image.jpg" alt="alt text" title="a title" >}}

Notes may occur anywhere. They take twice the horizontal space of thumbnails, and as much vertical space as needed. Notes shortcode has content, which is parsed as Markdown, e.g:

    {{< notes >}}
    Some Markdown text
    {{< /notes >}}

## Idiosyncrasies

While Hugo allows for extra flexibility, like non-Markdown albums, those are not tested with the PAL theme.

Due to specifics of Markdown rendering in Hugo the spacing around `photo` shortcodes is significant. If there are no empty lines (`\n\n` in *nix) between two shortcodes, then Hugo will render them with a gap, throwing off the layout.

## Licensing

The software code specific to this PAL theme for Hugo is licensed under the MIT License as described in the accompanying [LICENSE.md](LICENSE.md) file.

-----
