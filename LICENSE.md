# Copyright 2020-2022 Vlad Didenko

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

-----

The theme also depends on and includes a number of OSS and proprietary third party software and media assets. Here is the dependency list:

| component                | license                                                                                          |
| ------------------------ | ------------------------------------------------------------------------------------------------ |
| jQuery 3.5.1             | [MIT](https://github.com/jquery/jquery/blob/3.5.1/LICENSE.txt)                                   |
| lightGallery 1.7.0       | [GPL v3](https://github.com/sachinchoolur/lightGallery/blob/1.7.0/LICENSE.md)                    |
| Font Awesome Free 5.13.0 | [Font Awesome Free License](https://github.com/FortAwesome/Font-Awesome/blob/5.13.0/LICENSE.txt) |
| Comfortaa                | [Open Font License](https://fonts.google.com/specimen/Comfortaa?query=Comfo#license)             |
| Hugo 0.100.1              | [Apache License 2.0](https://github.com/gohugoio/hugo/blob/release-0.100.1/LICENSE)               |
