---
thumb: /pal/_/grey_landscape.jpg
title: All images
date: 2020-06-01
type: pal
---

{{< notes >}}
## Section 1

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
{{< /notes >}}

{{< photo path="/pal/blue.jpg">}}

{{< photo path="/pal/green_landscape.jpg">}}

{{< photo path="/pal/grey_portrait.jpg">}}

{{< photo path="/pal/green_portrait.jpg">}}

{{< photo path="/pal/blue_landscape.jpg">}}

{{< photo path="/pal/grey.jpg">}}

{{< photo path="/pal/green.jpg">}}

{{< photo path="/pal/grey_landscape.jpg">}}

{{< photo path="/pal/blue_portrait.jpg">}}

-----

{{< notes >}}
## Section 2

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
{{< /notes >}}

{{< photo path="/pal/blue.jpg">}}

{{< photo path="/pal/green_landscape.jpg">}}

{{< photo path="/pal/grey_portrait.jpg">}}

{{< photo path="/pal/green_portrait.jpg">}}

{{< photo path="/pal/blue_landscape.jpg">}}

{{< photo path="/pal/grey.jpg">}}

{{< photo path="/pal/green.jpg">}}

{{< photo path="/pal/grey_landscape.jpg">}}

{{< photo path="/pal/blue_portrait.jpg">}}

-----

{{< notes >}}
## Section 3

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
{{< /notes >}}

{{< photo path="/pal/blue.jpg">}}

{{< photo path="/pal/green_landscape.jpg">}}

{{< photo path="/pal/grey_portrait.jpg">}}

{{< photo path="/pal/green_portrait.jpg">}}

{{< photo path="/pal/blue_landscape.jpg">}}

{{< photo path="/pal/grey.jpg">}}

{{< photo path="/pal/green.jpg">}}

{{< photo path="/pal/grey_landscape.jpg">}}

{{< photo path="/pal/blue_portrait.jpg">}}
