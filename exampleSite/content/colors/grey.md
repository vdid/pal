---
thumb: /pal/_/grey.jpg
title: grey images
date: 2020-01-01
type: pal
---

{{< photo path="/pal/grey_portrait.jpg">}}

{{< photo path="/pal/grey.jpg">}}

{{< photo path="/pal/grey_landscape.jpg">}}
