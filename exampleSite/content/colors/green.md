---
thumb: "/pal/_/green.jpg"
title: "Green images"
date: 2020-02-01
type: "pal"
---

{{< photo path="/pal/green_landscape.jpg">}}

{{< photo path="/pal/green_portrait.jpg">}}

{{< photo path="/pal/green.jpg">}}
