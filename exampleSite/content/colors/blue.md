---
thumb: "/pal/_/blue.jpg"
title: "Blue images"
date: 2020-03-01
type: "pal"
---

{{< photo path="/pal/blue.jpg">}}

{{< photo path="/pal/blue_landscape.jpg">}}

{{< photo path="/pal/blue_portrait.jpg">}}
