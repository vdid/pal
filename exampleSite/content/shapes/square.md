---
thumb: "/pal/_/blue.jpg"
title: "Square images"
date: 2020-03-01
type: "pal"
---

{{< photo path="/pal/blue.jpg">}}

{{< photo path="/pal/grey.jpg">}}

{{< photo path="/pal/green.jpg">}}
