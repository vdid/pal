---
thumb: "/pal/_/green.jpg"
title: "Landscape images"
date: 2020-04-01
type: "pal"
---

{{< photo path="/pal/grey_landscape.jpg">}}

{{< photo path="/pal/green_landscape.jpg">}}

{{< photo path="/pal/blue_landscape.jpg">}}
