---
thumb: /pal/_/grey.jpg
title: Portrait images
date: 2020-02-01
type: pal
---

{{< photo path="/pal/green_portrait.jpg">}}

{{< photo path="/pal/grey_portrait.jpg">}}

{{< photo path="/pal/blue_portrait.jpg">}}
