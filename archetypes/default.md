+++
thumb = "/path/to/album/cover/image"
date = "2006-05-04"
title = "The Album title"
type = "pal"

+++

{{< photo path="path/to/first/FULL-SIZE/image/in/your/gallery.jpg" alt="" title="PASTE THIS SHORTCODE FOR EVERY IMAGE YOU HAVE IN THIS GALLERY" >}}

{{< photo path="path/to/second/FULL-SIZE/image/in/your/gallery.jpg" alt="" title="SOME SHORT DESCRIPTION. MARKDOWN **SUPPORTED**" >}}
